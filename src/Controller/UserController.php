<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[Route('/api/user')]
class UserController extends AbstractController
{

    private UserRepository $repo;
    
	function __construct(UserRepository $repo) {
	    $this->repo = $repo;
	}
    

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route(methods: 'POST')]
    public function register(Request $request, UserPasswordHasherInterface $hasher): Response {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->submit(json_decode($request->getContent(), true));

        if(!$form->isValid()) {
            return $this->json($form->getErrors(true), Response::HTTP_BAD_REQUEST);
        }
        $hash = $hasher->hashPassword($user, $user->getPassword());

        $user->setPassword($hash);
        $user->setRole('ROLE_USER');
        $this->repo->add($user, true);
        return $this->json($user, Response::HTTP_CREATED);
    }
}

<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Form\DogType;
use App\Repository\DogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FirstController extends AbstractController
{
    #[Route('/first', methods: 'GET')]
    public function index(): Response
    {

        return $this->json(['name' => 'Jean']);
    }

    #[Route('/first/dog', methods: 'GET')]
    public function firstDog(): Response {
        $dog = new Dog();
        $dog->setName('Fido')
        ->setBreed('Corgi')
        ->setBirthdate(new \DateTime('2021-01-02'));
        return $this->json($dog);
    }

    #[Route('/first/dog', methods: 'POST')]
    public function postDog(Request $request): Response {
        $dog = new Dog();
        
        $form = $this->createForm(DogType::class, $dog);
        
        $form->submit(json_decode($request->getContent(), true));     

        return $this->json($dog);
    }
}

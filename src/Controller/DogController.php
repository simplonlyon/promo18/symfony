<?php

namespace App\Controller;

use App\Entity\Dog;
use App\Form\DogType;
use App\Repository\DogRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/dog')]
class DogController extends AbstractController
{
    private DogRepository $repo;

    public function __construct(DogRepository $repo) {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all(): Response
    {
        return $this->json($this->repo->findAll());
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(Dog $dog): Response
    {
        return $this->json($dog);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request): Response
    {
        $dog = new Dog();
        $form = $this->createForm(DogType::class, $dog);
        $form->submit(json_decode($request->getContent(), true));

        $this->repo->add($dog, true);

        return $this->json($dog, Response::HTTP_CREATED);
    }



    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Dog $dog): Response
    {
        $this->repo->remove($dog, true);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }


    #[Route('/{id}', methods: 'PATCH')]
    public function update(Dog $dog, Request $request): Response
    {
        $form = $this->createForm(DogType::class, $dog);
        $form->submit(json_decode($request->getContent(), true), false);
        $this->repo->add($dog, true);

        return $this->json($dog);
    }
}
